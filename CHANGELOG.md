# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="0.0.1"></a>
## 0.0.1 (2017-08-23)


### Bug Fixes

* regex in gitlab-ci ([93be608](https://gitlab.com/MrWildcard/a-nice-app/commit/93be608))
* reseted package.jdon version number ([f4e9f6e](https://gitlab.com/MrWildcard/a-nice-app/commit/f4e9f6e))
* updated regexes branch names ([28eb6a5](https://gitlab.com/MrWildcard/a-nice-app/commit/28eb6a5))
* updated s3 bucket name ([e7711d5](https://gitlab.com/MrWildcard/a-nice-app/commit/e7711d5))


### Features

* added basic gitlab ci with test job ([733050b](https://gitlab.com/MrWildcard/a-nice-app/commit/733050b))
* added deploy to production job ([6b14028](https://gitlab.com/MrWildcard/a-nice-app/commit/6b14028))
* moved cache ci conf as global ([83f18e2](https://gitlab.com/MrWildcard/a-nice-app/commit/83f18e2))
* restricted test job regex'ed branch names ([7ca1055](https://gitlab.com/MrWildcard/a-nice-app/commit/7ca1055))
* try deploy with python image ([2e71787](https://gitlab.com/MrWildcard/a-nice-app/commit/2e71787))
* updated deploy script ([8dab638](https://gitlab.com/MrWildcard/a-nice-app/commit/8dab638))
* updated environments [skip ci] ([b429655](https://gitlab.com/MrWildcard/a-nice-app/commit/b429655))
* we use "release" only for release env ([2af9a08](https://gitlab.com/MrWildcard/a-nice-app/commit/2af9a08))
