import { createModule } from 'moducks';
import { call, select, put } from 'redux-saga/effects';
import { find } from 'lodash';
import client from '../common/client';

const initialState = {
  fetchedFavoritedGifs: []
};

const {
  selectModule,
  favorited,
  sagas: favoritedSagas,
  refreshFavoritedGifs,
  fetchFavoritedGifs,
  fetchFavoritedGifsSuccess,
  synchronouslyUpdateGifs
} = createModule(
  'favorited',
  {
    REFRESH_FAVORITED_GIFS: {
      *saga() {
        /**
         * There is a quiet annoying range of edge cases to cover when arriving on favorited page :
         * 1. you can come from the homepage where a search has already been fetched.
         *  - chances are that favorited gifs are already fetched and stored so you don't need to fetch them again
         * 2. you can already browsed favorited page from previous navigation and don't need to fetch any favorited
         * 3. you open Giphy App directly on the favorited page where everything you can base on are the string ids in localstorage.
         *
         * In any case, the most important part is to only fetch missing gifs from API to reduce server roundtrips.
         */
        const {
          fetchedGifs,
          localStoredFavoritedIds
        } = yield select(({ app, search }) => ({
          fetchedGifs: search.fetchedGifs,
          localStoredFavoritedIds: app.localStoredFavoritedIds
        }));

        const { fetchedFavoritedGifs } = yield selectModule.effect();

        if (
          !localStoredFavoritedIds.length &&
          fetchedFavoritedGifs.length > 0
        ) {
          return synchronouslyUpdateGifs([]);
        }

        /**
         * Replace gif IDs by already fetched ones found in search data and in this reducer.
         */
        const mappedFavoritedGifs = localStoredFavoritedIds.map(
          localStoredFavoritedId =>
            find(
              fetchedGifs,
              fetchedGif => fetchedGif.id === localStoredFavoritedId
            ) ||
            find(
              fetchedFavoritedGifs,
              fetchedFavoritedGif =>
                fetchedFavoritedGif.id === localStoredFavoritedId
            ) ||
            localStoredFavoritedId
        );

        /**
         * Keep gif object (the ones already fetched)
         */
        const alreadyFetchedFavoritedGifs = mappedFavoritedGifs.filter(
          localStoredFavorited => !(typeof localStoredFavorited === 'string')
        );

        /**
         * Keep remaining string IDs (the ones to fetch)
         */
        const favoritedGifIdsToFetch = mappedFavoritedGifs.filter(
          localStoredFavorited => typeof localStoredFavorited === 'string'
        );

        const totalAlreadyFetchedFavoritedGifs =
          alreadyFetchedFavoritedGifs.length;

        if (totalAlreadyFetchedFavoritedGifs) {
          yield put(synchronouslyUpdateGifs(alreadyFetchedFavoritedGifs));
        }

        if (favoritedGifIdsToFetch.length) {
          yield put(
            fetchFavoritedGifs(
              favoritedGifIdsToFetch,
              totalAlreadyFetchedFavoritedGifs > 0
            )
          );
        }
      }
    },
    FETCH_FAVORITED_GIFS: {
      creator: [
        ids => ({ ids }),
        (payload, mergeToExistingGifs = false) => ({ mergeToExistingGifs })
      ],
      *saga(action) {
        const { payload, meta } = action;

        const { data } = yield call(client.get, 'gifs', {
          params: { ids: payload.ids.join(',') }
        });

        yield put(fetchFavoritedGifsSuccess(data, meta.mergeToExistingGifs));
      }
    },

    FETCH_FAVORITED_GIFS_SUCCESS: {
      creator: [
        ({ data }) => ({ gifs: data }),
        (payload, mergeToExistingGifs = false) => ({ mergeToExistingGifs })
      ],
      reducer: (state, { payload, meta }) => ({
        ...state,
        fetchedFavoritedGifs: meta.mergeToExistingGifs
          ? state.fetchedFavoritedGifs.concat(payload.gifs)
          : payload.gifs
      })
    },

    SYNCHRONOUSLY_UPDATE_GIFS: {
      creator: gifs => ({ gifs }),
      reducer: (state, action) => ({
        ...state,
        fetchedFavoritedGifs: action.payload.gifs
      })
    }
  },
  initialState
);

export { favoritedSagas, refreshFavoritedGifs };

export default favorited;
