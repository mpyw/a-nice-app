import React from 'react';
import PropTypes from 'prop-types';
import GifItem from '../../common/components/GifItem/GifItem';

class Favorited extends React.PureComponent {
  static displayName = 'Favorited';

  static propTypes = {
    localStoredFavoritedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
    fetchedFavoritedGifs: PropTypes.arrayOf(PropTypes.object).isRequired,
    toggleGifFromFavorited: PropTypes.func.isRequired,
    refreshFavoritedGifs: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.props.refreshFavoritedGifs();
  }

  render() {
    return (
      <div className="favorited">
        {this.props.fetchedFavoritedGifs.map((gif, index) =>
          <GifItem
            key={`${gif.id}-${index}`}
            gifURL={gif.images.preview_gif.url}
            onClickHandler={() => {
              this.props.toggleGifFromFavorited(gif);
            }}
            isFavorited={this.props.localStoredFavoritedIds.includes(gif.id)}
          />
        )}
      </div>
    );
  }
}

export default Favorited;
