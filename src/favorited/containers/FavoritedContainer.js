import { connect } from 'react-redux';
import Favorited from '../components/Favorited';
import { refreshFavoritedGifs } from '../ducks';
import { toggleGifFromFavorited } from '../../app/ducks';

const mapStateToProps = ({ app, favorited }) => ({
  localStoredFavoritedIds: app.localStoredFavoritedIds,
  fetchedFavoritedGifs: favorited.fetchedFavoritedGifs
});

const mapDispatchToProps = {
  refreshFavoritedGifs,
  toggleGifFromFavorited
};

export default connect(mapStateToProps, mapDispatchToProps)(Favorited);
