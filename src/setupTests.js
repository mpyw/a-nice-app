/* eslint-disable import/no-extraneous-dependencies */
import { LocalStorage } from 'node-localstorage';

global.localStorage = new LocalStorage('node-localstorage');
