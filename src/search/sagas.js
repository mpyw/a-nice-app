import { takeEvery, put } from 'redux-saga/effects';
import { actionTypes } from 'redux-form';
import { searchRequest } from './ducks';

export const watchSearchFormInitialization = takeEvery(
  actionTypes.INITIALIZE,
  function* saga(action) {
    if (action.meta.form === 'SEARCH_FORM' && action.payload.searchTerms) {
      yield put(searchRequest(action.payload.searchTerms));
    }
  }
);
