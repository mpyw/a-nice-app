import { connect } from 'react-redux';
import Home from '../components/Home';
import { toggleGifFromFavorited } from '../../app/ducks';

const mapStateToProps = ({ search, app }) => ({
  fetchedGifs: search.fetchedGifs,
  localStoredFavoritedIds: app.localStoredFavoritedIds
});

const mapDispatchToProps = {
  toggleGifFromFavorited
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
