import React from 'react';
import PropTypes from 'prop-types';
import GifItem from '../../common/components/GifItem/GifItem';

class Home extends React.PureComponent {
  render() {
    return (
      <div className="home">
        <main role="main">
          {this.props.fetchedGifs.map((gif, index) =>
            <GifItem
              isFavorited={this.props.localStoredFavoritedIds.includes(gif.id)}
              key={`${gif.id}-${index}`}
              gifURL={gif.images.preview_gif.url}
              onClickHandler={() => {
                this.props.toggleGifFromFavorited(gif);
              }}
            />
          )}
        </main>
      </div>
    );
  }
}

Home.displayName = 'Home';

Home.propTypes = {
  fetchedGifs: PropTypes.arrayOf(PropTypes.object).isRequired,
  localStoredFavoritedIds: PropTypes.arrayOf(PropTypes.string).isRequired,
  toggleGifFromFavorited: PropTypes.func.isRequired
};

export default Home;
