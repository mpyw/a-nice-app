/* global localStorage */

import { call } from 'redux-saga/effects';
import { createModule } from 'moducks';
import { getArrayFromLocalStorage } from '../common/utils';

const initialState = {
  localStoredFavoritedIds: getArrayFromLocalStorage('favoritedIds')
};

const {
  app,
  sagas: appSagas,
  selectModule,
  toggleGifFromFavorited
} = createModule(
  'app',
  {
    TOGGLE_GIF_FROM_FAVORITED: {
      creator: gif => ({ gif }),
      reducer: (state, { payload }) => ({
        ...state,
        localStoredFavoritedIds: state.localStoredFavoritedIds.includes(
          payload.gif.id
        )
          ? state.localStoredFavoritedIds.filter(id => id !== payload.gif.id)
          : state.localStoredFavoritedIds.concat(payload.gif.id)
      }),
      *saga() {
        const { localStoredFavoritedIds } = yield selectModule.effect();

        if (localStoredFavoritedIds.length) {
          yield call(
            [localStorage, 'setItem'],
            'favoritedIds',
            localStoredFavoritedIds
          );
        } else {
          yield call([localStorage, 'removeItem'], 'favoritedIds');
        }
      }
    }
  },
  initialState
);

export { appSagas, toggleGifFromFavorited };

export default app;
