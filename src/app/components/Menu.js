import React from 'react';
import { Link } from 'react-router-dom';

const Menu = () =>
  <ul>
    <li>
      <Link to="/">Accueil</Link>
    </li>
    <li>
      <Link to="/favorited">Favoris</Link>
    </li>
  </ul>;

Menu.displayName = 'Menu';

export default Menu;
