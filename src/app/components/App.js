import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import Search from '../../search/containers/SearchContainer';
import Home from '../../home/containers/HomeContainer';
import Favorited from '../../favorited/containers/FavoritedContainer';
import Menu from './Menu';
import '../../styles/App.css';

class App extends Component {
  static displayName = 'App';

  render() {
    return (
      <div className="App">
        <Route path="/" component={Search} />
        <Route path="/" component={Menu} />

        <Route exact path="/" component={Home} />

        <Route exact path="/favorited" component={Favorited} />
      </div>
    );
  }
}

export default App;
