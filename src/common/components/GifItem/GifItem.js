import React from 'react';
import PropTypes from 'prop-types';
import cs from 'classnames';
import './GifItem.css';

class GifItem extends React.PureComponent {
  render() {
    const rootElementCSSClassnames = cs('gif-item', {
      favorited: this.props.isFavorited
    });

    return (
      <div className={rootElementCSSClassnames}>
        <button className="gif-actions" onClick={this.props.onClickHandler} />
        <img src={this.props.gifURL} alt="Giphy-Resultat-Recherche" />
      </div>
    );
  }
}

GifItem.defaultProps = {
  isFavorited: false
};

GifItem.propTypes = {
  isFavorited: PropTypes.bool.isRequired,
  gifURL: PropTypes.string.isRequired,
  onClickHandler: PropTypes.func.isRequired
};

export default GifItem;
