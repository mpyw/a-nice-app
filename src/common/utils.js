/* eslint-disable import/prefer-default-export */
/* global localStorage */

export const getArrayFromLocalStorage = (key, splitter = ',') => {
  const value = localStorage.getItem(key);

  return (value && value.split(splitter)) || [];
};
