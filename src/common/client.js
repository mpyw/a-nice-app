import axios from 'axios';

const client = axios.create({
  baseURL: '//api.giphy.com/v1/',
  params: {
    api_key: '7adc1aba405b49a3ba83dd677c00af97'
  }
});

export default client;
