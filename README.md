# A nice WebApp

[![build status](https://gitlab.com/MrWildcard/a-nice-app/badges/master/build.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/develop)
[![coverage report](https://gitlab.com/MrWildcard/a-nice-app/badges/master/coverage.svg)](https://gitlab.com/MrWildcard/a-nice-app/commits/master)
[![styled with prettier](https://img.shields.io/badge/styled_with-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

## You won't believe it

🤥

## Troubleshooting

- Debug what's going on in CI locally :

`
$ docker run -ti --rm -v `pwd`:/code -w /code DOCKER_IMAGE
`

Replace `DOCKER_IMAGE` by the your docker image name use in CI job.  